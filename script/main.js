// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після 
// натискання на вкладку відображався конкретний текст для потрібної вкладки. 
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст 
// має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть 
// додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті,
//  через такі правки не переставала працювати.
// object.onclick = function(){myScript};

function openTab(evt, tabName) {
    const tabContents = document.getElementsByClassName("tab-content");
    for (let i = 0; i < tabContents.length; i++) {
      tabContents[i].style.display = "none";
    }
  
    const tabLinks = document.getElementsByClassName("tabs-title");
    for (let i = 0; i < tabLinks.length; i++) {
      tabLinks[i].classList.remove("active");
    }
  
    const selectedTab = document.getElementById(tabName);
    selectedTab.style.display = "block";
  
    evt.currentTarget.classList.add("active");
  };


